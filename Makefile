COMPOSE:=docker-compose -p minotor -f src/docker-compose.yml

up:
	${COMPOSE} up -d

stop:
	${COMPOSE} stop

logs:
	${COMPOSE} logs -f --tail=20

logs_nofollow:
	${COMPOSE} logs --tail=20

auth/app/build:
	printf "$$(printenv)" > all.env
	docker run -u $$(id -u) -v $$(pwd)/src/auth:/app --env-file all.env -v ~/shares/composer:/composer composer/composer:1.2-alpine install --no-ansi --no-dev --no-interaction --no-progress --optimize-autoloader

build:
	${COMPOSE} build --pull

exec/ssl:
	${COMPOSE} exec front bash /opt/ssl/create.sh

dev/volume/create:
	docker volume create minotor-es-data
