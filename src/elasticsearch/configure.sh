#!/usr/bin/env bash

set -ex

ES_HOST=$1
TEMPLATE=$2

if [ -z "$ES_HOST" ];
then
  echo "<!> ES_HOST is not defined or empty! <!> \n"
  exit
fi

if [ -z "$TEMPLATE" ];
then
  echo "<!> TEMPLATE is not defined or empty! <!> \n"
  exit
fi

 curl -XDELETE ${ES_HOST}/_template/${TEMPLATE}

 curl -H 'Cookie: minotor=j6i1s5n8uosdrtj6tj6ncq7di4' -XPUT ${ES_HOST}/_template/${TEMPLATE} -d "$(cat ./template_${TEMPLATE}.json)"
