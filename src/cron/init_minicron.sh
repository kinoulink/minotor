#!/usr/bin/env bash

: ${MINICRON_DB_PATH:"/opt/minicron/lib/vendor/ruby/2.2.0/gems/minicron-0.9.7.1480251919/db/minicron.sqlite3"}

if [[ ! -f ${MINICRON_DB_PATH} ]]; then
  echo "[minicron] Creating database ..."
#  minicron db setup || exit 1
fi

echo "[minicron] Running server ..."
#minicron server start
