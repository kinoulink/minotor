<?php

namespace Auth;

use Composer\Script\Event;

class Scripts
{
  public static function generateBuildLog(Event $event)
  {
    //file_put_contents("./web/build.log", json_encode($_ENV));
    file_put_contents(__DIR__ . "/views/version.twig", sprintf("Build details: pipeline %s - branch %s - commit %s - date %s", $_ENV['CI_PIPELINE_ID'], $_ENV['CI_BUILD_REF_NAME'], $_ENV['CI_BUILD_REF'], date(DATE_RFC2822)));

    $event->getIO()->write("Wrote build.log and version.twig.");
  }
}
