<?php

    include "../vendor/autoload.php";

    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\RedirectResponse;

    $app = new \Silex\Application();

    $app->register(new Silex\Provider\SessionServiceProvider());

    $app->register(new Silex\Provider\TwigServiceProvider(), array(
        'twig.path' => __DIR__.'/../src/Auth/views',
    ));

    $app->register(new Silex\Provider\CsrfServiceProvider());

    $app['session.storage.options'] = [
        'name' => $_ENV['COOKIE_NAME'],
        'cookie_path' => '/',
        'cookie_domain' => $_ENV['COOKIE_DOMAIN'],
        'cookie_httponly' => false,
        'cookie_lifetime' => 3600 * 24,
        'cookie_secure' => false
    ];

    $app['oauth_client'] = $app->factory(function ()
    {
        return new Omines\OAuth2\Client\Provider\Gitlab([
            'clientId'          => $_ENV['OAUTH_CLIENT_ID'],
            'clientSecret'      => $_ENV['OAUTH_CLIENT_SECRET'],
            'redirectUri'       => sprintf('%s://%s/callback', 'http', $_ENV['OAUTH_DOMAIN'])
        ]);
    });

    $app['gitlab_api'] = $app->protect(function($accessToken, $uri)
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://gitlab.com'
        ]);

        $res = $client->request('GET', '/api/v3/' . $uri, [
            'headers' => [
                'User-Agent'        => 'Authy/1.0',
                'Accept'            => 'application/json',
                'Authorization'     => sprintf('Bearer %s', $accessToken)
            ]
        ]);

        if ($res->getHeader('content-type')[0] === 'application/json') {
            return \GuzzleHttp\json_decode((string)$res->getBody());
        }

        return (string)$res->getBody();
    });

    $app->get('/connect', function () use ($app)
    {
        $authClient = $app['oauth_client'];

        $authUrl = $authClient->getAuthorizationUrl();

        $app['session']->set('oauth2state', $authClient->getState());

        return new RedirectResponse($authUrl);
    });

    $app->get('/callback', function (Request $request) use ($app)
    {
        $authClient = $app['oauth_client'];

        // Retrieve access token
        $token = $authClient->getAccessToken('authorization_code', [
            'code' => $request->get('code'),
        ]);

        // Check user has project permission
        $projectPermission = $app['gitlab_api']($token->getToken(), sprintf('/projects/%d', $_ENV['GITLAB_PROJECT_ID']));

        if (empty($projectPermission)) throw new Exception("Gloups, no data from Gitlab API ;-(");

        if
        (
            (isset($projectPermission->permissions->group_access) &&
            !empty($projectPermission->permissions->group_access))
            ||
            (isset($projectPermission->permissions->project_access) &&
            !empty($projectPermission->permissions->project_access))
        )
        {
            $app['session']->set('token', $token);

            $user = $authClient->getResourceOwner($token);

            $app['session']->set('user', $user->toArray());

            return new RedirectResponse('/');
        }

        throw new Exception('It seems you dont have permission for this ressource!');
    });

    $app->get('/', function() use ($app)
    {
        $session = $app['session'];

        if ($session->has('user')) {
            return $app['twig']->render('index.twig', [
                'user' => $session->get('user'),
            ]);
        } else {
            return $app['twig']->render('public.twig');
        }
    });

    $app->get('/logout', function() use ($app)
    {
        $app['session']->clear();

       return new RedirectResponse('/');
    });

    $app->get('/__auth/check', function() use ($app)
    {
        if ($app['session']->has('user'))
        {
            return new \Symfony\Component\HttpFoundation\Response('Hello ' . $app['session']->get('user')['name'], 200);
        }
        else
        {
            return new \Symfony\Component\HttpFoundation\Response('Not connected', 403);
        }
    });

    $app->error(function (\Exception $e, $code) use ($app)
    {
        return $app['twig']->render('error.twig', [
            'error' => $e
        ]);
    });

    $app->run();
