## Services

- Reverse proxy - Nginx 1.10 - HTTP & HTTPS - SSL with LetsEncrypt
- Elasticsearch - 5.2
- Kibana - 5.2
- Logstash 5.2
- Authentication - Php app home-made for Gitlab Oauth - Use Nginx ``auth_request`` directive

## Misc

- https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-14-04
