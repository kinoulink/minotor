<?php

$metrics = [
    'metric_cpu' => rand(0,100),
    'metric_ram' => rand(300, 3000),
    'metric_disk' => rand(300, 3000),
];

$devices = [
  'alpha1' => [
      'client' => 'tom',
      'version' => 'v2'
  ],
    'alpha2' => [
        'client' => 'tom',
        'version' => 'v2'
    ],
    'alpha3' => [
        'client' => 'fred',
        'version' => 'beta'
    ],
];

$events = [];

foreach($devices as $device)
{
    $events[] = [
        'date' => time(),
        'device' => $device,
        'metrics' => $metrics
    ];
}

$ch = curl_init('http://localhost:8080');

for ($i = 0; $i < 100000; $i++)
{
    sendEvent();
}

function sendEvent()
{
    global $events, $ch;

    foreach($events as &$device)
    {
        foreach ($device['metrics'] as $p => &$v)
        {
            $v += rand(-5, 5);

            $v = max($v, 0);
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($device));

        curl_exec($ch);

        $device['date'] -= 60;
    }
}